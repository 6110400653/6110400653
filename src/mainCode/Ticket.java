package mainCode;

import javafx.stage.Stage;

import java.util.ArrayList;

public class Ticket {
    private ArrayList<String> seat = new ArrayList<>() ;
    private String movieName ;
    private String showTime ;
    private Theatre theatre ;
    private Movie movie = new Movie() ;
    private int theatreNumber ;
    private double price ;

    public Theatre getTheatre() {
        return theatre;
    }

    public void setTheatre(Theatre theatre) {
        this.theatre = theatre;
        this.setTheatreNumber(theatre.getNumber());
    }

    public Ticket() {
        this.showTime = "" ;
        this.movieName = "" ;
    }

    public Ticket(String movieName, String showTime){
        this.movieName = movieName ;
//        this.seat = seat ;
        this.showTime = showTime ;
    }

    public Ticket(ArrayList<String> seat, String movieName, String showTime, Theatre theatre){
        this.movieName = movieName ;
        this.showTime = showTime ;
        this.seat = seat ;
        this.theatre = theatre ;
        this.theatreNumber = theatre.getNumber() ;
    }

    public Ticket(String movieName, String showTime, Theatre theatre){
        this.movieName = movieName ;
        this.showTime = showTime ;
        this.theatre = theatre ;
        this.theatreNumber = theatre.getNumber() ;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void addSeat(String seat){
        this.seat.add(seat) ;
    }

//    public void setSeat(ArrayList<String> seat) {
//        this.seat = seat;
//    }

    public String getShowTime() {
        return showTime;
    }

    public void setShowTime(String showTime) {
        this.showTime = showTime;
    }

    public String getMovieName() {
        return movieName;
    }

    public ArrayList<String> getSeat() {
        return seat;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setTheatreNumber(int theatreNumber) {
        this.theatreNumber = theatreNumber;
    }

    public int getTheatreNumber() {
        return theatreNumber;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    //    public String toString(){
//        return
//    }
}
