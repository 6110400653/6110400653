package mainCode;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class CreditController {

    @FXML
    Button backBtn ;

    @FXML
    public void backBtnAction(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
//        seatSelectController.setT1(new Ticket("Avanger", "A1", 1));
        stage.show();
    }
}
