package mainCode;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class NormalSeatController {
    private Ticket t1 = new Ticket() ;
    private MasterSeat seatData = new MasterSeat() ;
    private Customer customer ;
    private BookingData bookingData ;
    private ArrayList<Customer> customers ;

    @FXML
    private ArrayList<ToggleButton> seat = new ArrayList<>() ;

    @FXML
    private ArrayList<Label> logs = new ArrayList<>() ;

    @FXML
    private Label ticketInfo ;

    @FXML
    AnchorPane pane ;

    public void load(){
        int i = 0 ;
        while(i < 40){
            seat.add(new ToggleButton()) ;
            i++ ;
        }
    }

    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                int i = 1 ;
                int numRow = 1 ;
                int x = seatData.getNormalSeatX() ;
                int y = seatData.getNormalSeatY() ;
                int imageNum = 0 ;
                String image = "" ;
                load() ;
                String s1 = "" ;
                for(ToggleButton s: seat){
                    image = "" ;
//            s = new ToggleButton() ;
                    s.setLayoutX(x);
                    s.setLayoutY(y);
                    s.setPrefSize(seatData.getNormalSeatWidth(), seatData.getNormalSeatHeight());
                    s.setOnAction(this::swapAction);
                    imageNum = i % 8 ;
                    if(imageNum == 0){
                        imageNum = 8 ;
                    }
//                    System.out.println("i = "+ imageNum);
                    image += "/asset/"+imageNum+"normal.png" ;
                    ImageView m = new ImageView(image) ;
                    ImageView m2 = new ImageView(seatData.getDisableSeat()) ;
                    m2.setFitHeight(seatData.getNormalSeatHeight()-15);
                    m2.setFitWidth(seatData.getNormalSeatWidth()-20);
                    m.setFitHeight(seatData.getNormalSeatHeight()-15);
                    m.setFitWidth(seatData.getNormalSeatWidth()-20);
                    s.setGraphic(m);
                    s.setId(i+"");
                    if(i <= 8){
                        System.out.println(t1.getMovieName());
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "A"+numRow)){
                            s.setDisable(true);
                            s.setGraphic(m2);
                        }
                    }
                    else if(i <= 16){
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "B"+numRow)){
                            s.setDisable(true);
                            s.setGraphic(m2);
                        }
                    }
                    else if(i <= 24){
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "C"+numRow)){
                            s.setDisable(true);
                            s.setGraphic(m2);
                        }
                    }
                    else if(i <= 32){
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "D"+numRow)){
                            s.setDisable(true);
                            s.setGraphic(m2);
                        }
                    }
                    else if(i <= 40){
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "E"+numRow)){
                            s.setDisable(true);
                            s.setGraphic(m2);
                        }
                    }

//            s.getChildrenUnmodifiable().add(new ImageView("/asset/normalSeat.png")) ;
                    x += seatData.getNormalSeatWidth() ;
                    if(i%8 == 0){
                        numRow = 0 ;
                        x = seatData.getNormalSeatX() ;
                        y += seatData.getNormalSeatHeight() ;
                    }
                    pane.getChildren().add(s) ;
                    i += 1 ;
                    numRow++ ;
                }

            }

            private void swapAction(ActionEvent e) {
                ToggleButton s = (ToggleButton) e.getSource() ;
                if(s.isSelected()){
                    ImageView imageView = new ImageView(seatData.getNormalSeatSelectedImage()) ;
                    imageView.setFitWidth(seatData.getNormalSeatWidth()-20);
                    imageView.setFitHeight(seatData.getNormalSeatHeight()-15);
                    s.setGraphic(imageView);
                }
                else{
//                    "/asset/"+s.getId()+"normal.png"
//                    System.out.println("s = "+ s.getId());
                    int imageNum = Integer.parseInt(s.getId()) % 8 ;
                    if(imageNum == 0){
                        imageNum = 8 ;
                    }
                    ImageView imageView = new ImageView("/asset/"+imageNum+"normal.png") ;
                    imageView.setFitWidth(seatData.getNormalSeatWidth()-20);
                    imageView.setFitHeight(seatData.getNormalSeatHeight()-15);
                    s.setGraphic(imageView);
                }
            }
        });


    }

    public void setT1(Ticket t1) {
        this.t1 = t1;
//        ticketInfo.setText("Theater: "+t1.getTheaterNumber()+"  Movie: "+t1.getMovieName()+"  Showtime: "+t1.getShowTime());
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
//        System.out.println(customer.getName()+" normalSeat");
    }

    public void setTicketInfo(String text){
        ticketInfo.setText(text);
    }

    public void setBookingData(BookingData bookingData) {
        this.bookingData = bookingData;
        int i = 0 ;
        for(Ticket t: bookingData.getTickets()){
            System.out.println(""+t.getShowTime());
            i++ ;
        }
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    @FXML
    public void backBtnAction(ActionEvent event) throws IOException {
        Button b2 = (Button) event.getSource() ;
        Stage stage = (Stage) b2.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        HomeController homeController = loader.getController() ;
        homeController.setT1(t1);
//        customer.addTicket(t1);
//        bookingData.addData(t1);
        homeController.setCustomer(customer);
        homeController.setBookingData(bookingData);
        stage.show();
    }

    @FXML
    public void acceptBtnAction(ActionEvent event) throws IOException {
        int numSeat = 0 ;
        double price = 0 ;
        int i = 1 ;
        int x = 1 ;
        for(ToggleButton s: seat){
            if(s.isSelected() && i <= 8){
                t1.addSeat("A"+x);
                numSeat++ ;
            }
            else if(s.isSelected() && i <= 16){
                t1.addSeat("B"+x);
                numSeat++ ;
            }
            else if(s.isSelected() && i <= 24){
                t1.addSeat("C"+x);
                numSeat++ ;
            }
            else if(s.isSelected() && i <= 32){
                t1.addSeat("D"+x);
                numSeat++ ;
            }
            else if(s.isSelected() && i <= 40){
                t1.addSeat("E"+x);
                numSeat++ ;
            }

            if(x%8 == 0){
                x = 0 ;
            }
            i++ ;
            x++ ;
        }

        Button b2 = (Button) event.getSource();
        Stage stage = (Stage) b2.getScene().getWindow() ;
        FXMLLoader loader2 = new FXMLLoader(getClass().getResource("ticketShow.fxml")) ;
        stage.setScene(new Scene(loader2.load(), 720, 400));
        TicketShowController ticketShowController = loader2.getController() ;
        price += seatData.getNormalSeatPrice() * numSeat ;
        if(t1.getTheatre().getSystem().equals("4K")){
            price += 30 ;
        }
        else if(t1.getTheatre().getSystem().equals("3D")){
            price += 50 ;
        }
        t1.setPrice(price);
        ticketShowController.setT1(t1);
        customer.addTicket(t1);
        bookingData.addData(t1);
        ticketShowController.setCustomer(customer);
        ticketShowController.setBookingData(bookingData);
        ticketShowController.setCustomers(customers);
        writeFile();
//        System.out.println(t1.get);
//        System.out.println(t1.getMovieName());
        stage.show();
    }

    public void writeFile() throws IOException {
        String customerData = "" ;
        for(Customer customer:customers){
            customerData += customer.getName() + "," + customer.getId() + "," + customer.getPass() + "," + customer.getMail() + "\n" ;
            for(Ticket ticket:customer.getTickets()){
                customerData += ticket.getTheatreNumber() + "," + ticket.getShowTime() + "," ;
                for(String seat:ticket.getSeat()){
                    customerData += seat + " " ;
                }
                customerData += "\n" ;
            }
        }

        FileWriter writer = new FileWriter("BookingData.csv");
        writer.write(customerData);
        writer.close();
//        System.out.println(customerData);
    }

    @FXML
    public void swapAction(ActionEvent e){
        ToggleButton s = (ToggleButton) e.getSource() ;
        if(s.isSelected()){
            ImageView imageView = new ImageView(seatData.getNormalSeatSelectedImage()) ;
            imageView.setFitWidth(seatData.getNormalSeatWidth()-20);
            imageView.setFitHeight(seatData.getNormalSeatHeight()-15);
            s.setGraphic(imageView);
        }
        else{
            ImageView imageView = new ImageView(seatData.getNormalSeatImage()) ;
            imageView.setFitWidth(seatData.getNormalSeatWidth()-20);
            imageView.setFitHeight(seatData.getNormalSeatHeight()-15);
            s.setGraphic(imageView);
        }
    }

}
