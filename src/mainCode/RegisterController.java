package mainCode;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class RegisterController {

    private ArrayList<Customer> customers = new ArrayList<>() ;

    @FXML
    TextField username, password, fullname, email ;

    @FXML
    Label registerLog ;

    @FXML
    Button submitBtn, backBtn ;

    public void submitBtnAction(ActionEvent e) throws IOException {
        String user = username.getText() ;
        String pass = password.getText() ;
        String name = fullname.getText() ;
        String mail = email.getText() ;

        for(Customer c:customers){
            if(c.getId().equals(user)){
                registerLog.setText("Username already use");
                return;
            }
            else if(c.getMail().equals(mail)){
                registerLog.setText("E-mail already use");
                return;
            }
        }

        customers.add(new Customer(name, user, pass, mail)) ;
        writeFile();

        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
//        seatSelectController.setT1(new Ticket("Avanger", "A1", 1));
        stage.show();

    }

    public void backBtnAction(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
//        seatSelectController.setT1(new Ticket("Avanger", "A1", 1));
        stage.show();
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public void writeFile() throws IOException {
        String customerData = "" ;
        for(Customer customer:customers){
            customerData += customer.getName() + "," + customer.getId() + "," + customer.getPass() + "," + customer.getMail() + "\n" ;
            for(Ticket ticket:customer.getTickets()){
                customerData += ticket.getTheatreNumber() + "," + ticket.getShowTime() + "," ;
                for(String seat:ticket.getSeat()){
                    customerData += seat + " " ;
                }
                customerData += "\n" ;
            }
        }

        FileWriter writer = new FileWriter("BookingData.csv");
        writer.write(customerData);
        writer.close();
//        System.out.println(customerData);
    }
}
