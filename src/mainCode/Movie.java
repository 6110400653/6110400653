package mainCode;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

import java.util.ArrayList;

public class Movie {
    private String name ;
    private String description ;
    private int theatreNumber ;
    private String trailer ;
    private String length ;
    private String poster ;

    public Movie(){

    }

    public Movie(String name, String description, int theatreNumber, String trailer, String length, String poster) {
        this.name = name;
        this.description = description;
        this.theatreNumber = theatreNumber;
        this.trailer = trailer ;
        this.length = length ;
        this.poster = poster ;
    }

    public String getLength() {
        return length;
    }

    public String getTrailer() {
        return trailer;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getTheatreNumber() {
        return theatreNumber;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }
}
