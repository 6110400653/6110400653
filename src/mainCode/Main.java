package mainCode;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Parent root = FXMLLoader.load(getClass().getResource("home.fxml"));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml")) ;
        primaryStage.setTitle("MOOVIE APP");
        primaryStage.setScene(new Scene(loader.load(), 720, 400));
//        LoginController.
//        HomeController homeController = loader.getController() ;
//        homeController.setCustomer(new Customer("Pang"));
//        homeController.setBookingData(new BookingData());
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
