package mainCode;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

//import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;

import javafx.event.ActionEvent ;

public class HomeController {
    private Ticket t1 = new Ticket();
    private ArrayList<Theatre> theatres = new ArrayList<>() ;
    private Customer customer ;
    private BookingData bookingData ;
    private ArrayList<Customer> customers ;

    @FXML
    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12, btn13, logoutBtn, info1Btn, info2Btn, info3Btn, info4Btn ;

    @FXML
    Label username ;

    @FXML
//    public void goToBookingScreen(ActionEvent event){
//        try {
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("seatSelect.fxml")) ;
//            Parent root = (Parent) loader.load() ;
//            SeatSelectController seatSelectController = loader.getController() ;
//            seatSelectController.setT1(new Ticket(t1.getMovieName(), t1.getShowTime(), t1.getTheaterNumber()));
//            seatSelectController.setTicketInfo("Theater: "+t1.getTheaterNumber()+"  Movie: "+t1.getMovieName()+"  Showtime: "+t1.getShowTime());
//            Stage stage = new Stage() ;
//            stage.setScene(new Scene(root));
//            stage.show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String info1 = "After the devastating events of Avengers: Infinity War (2018), " +
                        "the universe is in ruins. With the help of remaining allies, " +
                        "the Avengers assemble once more in order to reverse Thanos' " +
                        "actions and restore balance to the universe." ;
                String info2 = "As the Avengers and their allies have continued to " +
                        "protect the world from threats too large for any one hero to handle, " +
                        "a new danger has emerged from the cosmic shadows: Thanos. A despot of " +
                        "intergalactic infamy, his goal is to collect all six Infinity Stones, " +
                        "artifacts of unimaginable power, and use them to inflict his twisted will on " +
                        "all of reality. Everything the Avengers have fought for has led up to this moment, the " +
                        "fate of Earth and existence has never been more uncertain." ;
                String info3 = "Tony Stark creates the Ultron Program to protect the world, " +
                        "but when the peacekeeping program becomes hostile, " +
                        "The Avengers go into action to try and defeat a virtually " +
                        "impossible enemy together. Earth's mightiest heroes must " +
                        "come together once again to protect the world from global extinction." ;
                String info4 = "Loki, the adopted brother of Thor, teams-up with the Chitauri " +
                        "Army and uses the Tesseract's power to travel from Asgard to Midgard " +
                        "to plot the invasion of Earth and become a king. The director of the " +
                        "agency S.H.I.E.L.D., Nick Fury, sets in motion project Avengers, joining " +
                        "Tony Stark a.k.a. the Iron Man; Steve Rogers, a.k.a. Captain America; Bruce Banner, " +
                        "a.k.a. The Hulk; Thor; Natasha Romanoff, a.k.a. Black Widow; and Clint Barton, a.k.a. " +
                        "Hawkeye, to save the world from the powerful Loki and the alien invasion." ;
                theatres.add(new Theatre(1, new Movie("Avangers 4", info1, 1, "trailer/avangers4.mp4", "3.05", "/asset/avangers4.jpg"), "Normal")) ;
                theatres.add(new Theatre(2, new Movie("Avangers 3", info2, 2, "trailer/avangers3.mp4", "2.30", "/asset/avangers3.jpg"), "4K")) ;
                theatres.add(new Theatre(3, new Movie("Avangers 2", info3, 3, "trailer/avangers2.mp4", "1.45", "/asset/avangers2.jpg"), "4K")) ;
                theatres.add(new Theatre(4, new Movie("Avangers", info4, 4, "trailer/avangers.mp4", "1.35", "/asset/avangers.jpg"), "3D")) ;

                theatres.get(0).addShowTimes("13:00");
                theatres.get(0).addShowTimes("16:00");
                theatres.get(0).addShowTimes("20:00");

                theatres.get(1).addShowTimes("12:00");
                theatres.get(1).addShowTimes("14:00");
                theatres.get(1).addShowTimes("16:00");
                theatres.get(1).addShowTimes("21:00");

                theatres.get(2).addShowTimes("12:30");
                theatres.get(2).addShowTimes("14:30");
                theatres.get(2).addShowTimes("16:30");
                theatres.get(2).addShowTimes("19:00");

                theatres.get(3).addShowTimes("21:00");
                theatres.get(3).addShowTimes("23:00");

                username.setText("Hello :" + customer.getName());
            }
        });

//        System.out.println(customer.getName());
//        customer = new Customer("Pang") ;
    }

    public void goToBookingScreen(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mixedSeat.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        MixedSeatController mixedSeatController = loader.getController() ;
//        seatSelectController.setT1(new Ticket("Avanger", "A1", 1));
        mixedSeatController.setT1(new Ticket(t1.getMovieName(), t1.getShowTime(), t1.getTheatre()));
        mixedSeatController.setTicketInfo("Theater: "+t1.getTheatre().getNumber()+"  Movie: "+t1.getMovieName()+"  Showtime: "+t1.getShowTime());
        mixedSeatController.setCustomer(customer);
        mixedSeatController.setBookingData(bookingData);
        mixedSeatController.setCustomers(customers);
        stage.show();
    }

    public void goToBookingScreenNormalSeat(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("normalSeat.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        NormalSeatController normalSeatController = loader.getController() ;
//        seatSelectController.setT1(new Ticket("Avanger", "A1", 1));
        normalSeatController.setT1(new Ticket(t1.getMovieName(), t1.getShowTime(), t1.getTheatre()));
        normalSeatController.setTicketInfo("Theater: "+t1.getTheatre().getNumber()+"  Movie: "+t1.getMovieName()+"  Showtime: "+t1.getShowTime());
        normalSeatController.setCustomer(customer);
        normalSeatController.setBookingData(bookingData);
        normalSeatController.setCustomers(customers);
//        stage.setScene(new Scene(loader.load(), 720, 400));
        stage.show();
    }

    public void btn1Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger4 (Normal)");
        t1.setMovieName(theatres.get(0).getMovieName().getName()+"("+theatres.get(0).getSystem()+")");
        t1.setMovie(theatres.get(0).getMovieName());
        t1.setShowTime("13:00");
        t1.setTheatre(theatres.get(0));
        System.out.println(t1.getTheatre().getNumber()+"");
        goToBookingScreenNormalSeat(event); ;
    }

    public void btn2Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger4 (Normal)");
        t1.setMovieName(theatres.get(0).getMovieName().getName()+"("+theatres.get(0).getSystem()+")");
        t1.setMovie(theatres.get(0).getMovieName());
        t1.setShowTime("16:00");
        t1.setTheatre(theatres.get(0));
        goToBookingScreenNormalSeat(event); ;
    }
    public void btn3Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger4 (Normal)");
        t1.setMovieName(theatres.get(0).getMovieName().getName()+"("+theatres.get(0).getSystem()+")");
        t1.setMovie(theatres.get(0).getMovieName());
        t1.setShowTime("20:00");
        t1.setTheatre(theatres.get(0));
        goToBookingScreenNormalSeat(event); ;
    }
    public void btn4Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger3 (4K)");
        t1.setMovieName(theatres.get(1).getMovieName().getName()+"("+theatres.get(1).getSystem()+")");
        t1.setMovie(theatres.get(1).getMovieName());
        t1.setShowTime("12:00");
        t1.setTheatre(theatres.get(1));
//        t1.setTheaterNumber(2);
        goToBookingScreenNormalSeat(event); ;
    }
    public void btn5Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger3 (4K)");
        t1.setMovieName(theatres.get(1).getMovieName().getName()+"("+theatres.get(1).getSystem()+")");
        t1.setMovie(theatres.get(1).getMovieName());
        t1.setShowTime("14:00");
        t1.setTheatre(theatres.get(1));
//        t1.setTheaterNumber(2);
        goToBookingScreenNormalSeat(event); ;
    }
    public void btn6Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger3 (4K)");
        t1.setMovieName(theatres.get(1).getMovieName().getName()+"("+theatres.get(1).getSystem()+")");
        t1.setMovie(theatres.get(1).getMovieName());
        t1.setShowTime("16:00");
        t1.setTheatre(theatres.get(1));
//        t1.setTheaterNumber(2);
        goToBookingScreenNormalSeat(event); ;
    }
    public void btn7Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger3 (4K)");
        t1.setMovieName(theatres.get(1).getMovieName().getName()+"("+theatres.get(1).getSystem()+")");
        t1.setMovie(theatres.get(1).getMovieName());
        t1.setShowTime("21:00");
        t1.setTheatre(theatres.get(1));
//        t1.setTheaterNumber(2);
        goToBookingScreenNormalSeat(event); ;
    }
    public void btn8Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger2 (4K)");
        t1.setMovieName(theatres.get(2).getMovieName().getName()+"("+theatres.get(2).getSystem()+")");
        t1.setMovie(theatres.get(2).getMovieName());
        t1.setShowTime("12:30");
        t1.setTheatre(theatres.get(2));
//        t1.setTheaterNumber(3);
        goToBookingScreen(event) ;
    }
    public void btn9Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger2 (4K)");
        t1.setMovieName(theatres.get(2).getMovieName().getName()+"("+theatres.get(2).getSystem()+")");
        t1.setMovie(theatres.get(2).getMovieName());
        t1.setShowTime("14:30");
        t1.setTheatre(theatres.get(2));
//        t1.setTheaterNumber(3);
        goToBookingScreen(event) ;
    }
    public void btn10Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger2 (4K)");
        t1.setMovieName(theatres.get(2).getMovieName().getName()+"("+theatres.get(2).getSystem()+")");
        t1.setMovie(theatres.get(2).getMovieName());
        t1.setShowTime("16:30");
        t1.setTheatre(theatres.get(2));
//        t1.setTheaterNumber(3);
        goToBookingScreen(event) ;
    }
    public void btn11Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger2 (4K)");
        t1.setMovieName(theatres.get(2).getMovieName().getName()+"("+theatres.get(2).getSystem()+")");
        t1.setMovie(theatres.get(2).getMovieName());
        t1.setShowTime("19:00");
        t1.setTheatre(theatres.get(2));
//        t1.setTheaterNumber(3);
        goToBookingScreen(event) ;
    }
    public void btn12Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger (3D)");
        t1.setMovieName(theatres.get(3).getMovieName().getName()+"("+theatres.get(3).getSystem()+")");
        t1.setMovie(theatres.get(3).getMovieName());
        t1.setShowTime("21:00");
        t1.setTheatre(theatres.get(3));
//        t1.setTheaterNumber(4);
        goToBookingScreen(event) ;
    }
    public void btn13Action(ActionEvent event) throws IOException {
//        t1.setMovieName("Avanger (3D)");
        t1.setMovieName(theatres.get(3).getMovieName().getName()+"("+theatres.get(3).getSystem()+")");
        t1.setMovie(theatres.get(3).getMovieName());
        t1.setShowTime("23:00");
        t1.setTheatre(theatres.get(3));
//        t1.setTheaterNumber(4);
        goToBookingScreen(event) ;
    }

    public void logoutBtnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        stage.show();
    }

    public void goToInfoScreen(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("movieInfo.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        MovieInfoController movieInfoController = loader.getController() ;
//        seatSelectController.setT1(new Ticket("Avanger", "A1", 1));
        movieInfoController.setT1(t1);
        movieInfoController.setCustomer(customer);
        movieInfoController.setBookingData(bookingData);
        movieInfoController.setCustomers(customers);
        stage.show();

    }

    public void info1BtnAction(ActionEvent event) throws IOException {
        t1.setMovie(theatres.get(0).getMovieName());
        goToInfoScreen(event) ;
    }
    public void info2BtnAction(ActionEvent event) throws IOException {
        t1.setMovie(theatres.get(1).getMovieName());
        goToInfoScreen(event) ;
    }
    public void info3BtnAction(ActionEvent event) throws IOException {
        t1.setMovie(theatres.get(2).getMovieName());
        goToInfoScreen(event) ;
    }
    public void info4BtnAction(ActionEvent event) throws IOException {
        t1.setMovie(theatres.get(3).getMovieName());
        goToInfoScreen(event) ;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
//        System.out.println(customer.getName());
    }

    public void setBookingData(BookingData bookingData) {
        this.bookingData = bookingData;
    }

    public void setT1(Ticket t1) {
        this.t1 = t1;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

}
