package mainCode;

import java.util.ArrayList;

public class Customer {
    private String name ;
    private String id ;
    private String pass ;
    private String mail ;

    public Customer(String name, String id, String pass, String mail) {
        this.name = name;
        this.id = id ;
        this.pass = pass ;
        this.mail = mail ;
    }

    public Customer(){

    }

    private ArrayList<Ticket> tickets = new ArrayList<>() ;

    public void addTicket(Ticket ticket){
        tickets.add(ticket) ;
    }

    public ArrayList<Ticket> getTickets() {
        return tickets;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getPass() {
        return pass;
    }

    public String getMail() {
        return mail;
    }
}
