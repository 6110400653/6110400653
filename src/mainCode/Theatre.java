package mainCode;

import java.util.ArrayList;
import java.util.Collections;

public class Theatre {
    private int number ;
    private Movie movieName ;
    private ArrayList<String> showTimes = new ArrayList<>() ;
    private String system ;

    public Theatre(int number, Movie movieName, String system) {
        this.number = number;
        this.movieName = movieName;
        this.system = system ;
    }

    public void addShowTimes(String time){
        showTimes.add(time) ;
        Collections.sort(showTimes);
    }

    public String getSystem() {
        return system;
    }

    public int getNumber() {
        return number;
    }

    public Movie getMovieName() {
        return movieName;
    }

    public ArrayList<String> getShowTimes() {
        return showTimes;
    }


}
