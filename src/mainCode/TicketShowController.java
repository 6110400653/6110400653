package mainCode;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class TicketShowController {
    private Ticket t1 = new Ticket() ;
    private Customer customer ;
    private BookingData bookingData ;
    private ArrayList<Customer> customers ;

    @FXML
    Button backBtn ;

    @FXML
    Label theaterNumber, movieName, showTime, seat, price ;

    @FXML
    public void initialize(){

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                theaterNumber.setText(String.valueOf(t1.getTheatre().getNumber()));
                movieName.setText(t1.getMovieName());
                showTime.setText(t1.getShowTime());
                String text = "" ;
                for (String t:t1.getSeat()) {
                    text += t +" " ;
                }
                seat.setText(text);
                price.setText(t1.getPrice()+" Baht");

//                customer.addTicket(t1);
                System.out.println("name: "+customer.getName());
                for(Ticket ticket:customer.getTickets()){
                    for(String s:ticket.getSeat()){
                        System.out.println("movie name: "+ticket.getMovieName()+ " seat:" + s);
                    }
                }
            }
        });

//        display() ;
    }


    public void setT1(Ticket t1){
        this.t1 = t1 ;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        System.out.println(customer.getName()+" ticket");
    }

    public void setBookingData(BookingData bookingData) {
        this.bookingData = bookingData;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public void display(){
        theaterNumber.setText(String.valueOf(t1.getTheatre().getNumber()));
        movieName.setText(t1.getMovieName());
        showTime.setText(t1.getShowTime());
        String text = "" ;
        for (String t:t1.getSeat()) {
            text += t ;
        }
        seat.setText(text);
    }

    @FXML
    public void backBtnAction(ActionEvent event) throws IOException {
        Button b2 = (Button) event.getSource() ;
        Stage stage = (Stage) b2.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        HomeController homeController = loader.getController() ;
        homeController.setCustomer(customer);
        homeController.setBookingData(bookingData);
        homeController.setCustomers(customers);
        stage.show();
    }
}
