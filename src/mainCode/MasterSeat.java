package mainCode;

public class MasterSeat {
    private int normalSeatX = 50 ;
    private int normalSeatY = 45 ;
    private int normalSeatWidth = 60 ;
    private int normalSeatHeight = 45 ;
    private int normalSeatPrice = 200 ;
    private String normalSeatImage = "/asset/normalSeat.png" ;
    private String normalSeatSelectedImage = "/asset/normalSeatSelected.png" ;

    private int loveSeatX = 185 ;
    private int loveSeatY = 30 ;
    private int loveSeatWidth = 60 ;
    private int loveSeatHeight = 40 ;
    private int loveSeatPrice = 400 ;
    private String loveSeatImage = "/asset/loveSeat.png" ;
    private String loveSeatSelectedImage = "/asset/loveSeatSelected.png" ;

    private int vipSeatX = 125 ;
    private int vipSeatY = 70 ;
    private int vipSeatWidth = 60 ;
    private int vipSeatHeight = 40 ;
    private int vipSeatPrice = 250 ;
    private String vipSeatImage = "/asset/vipSeat.png" ;
    private String vipSeatSelectedImage = "/asset/vipSeatSelected.png" ;

    private String disableSeat = "/asset/bookedSeat.png" ;
    private String disableSeatLove = "/asset/bookedLoveSeat.png" ;

    public String getDisableSeat() {
        return disableSeat;
    }

    public String getDisableSeatLove() {
        return disableSeatLove;
    }

    public int getVipSeatHeight() {
        return vipSeatHeight;
    }

    public int getVipSeatPrice() {
        return vipSeatPrice;
    }

    public String getVipSeatSelectedImage() {
        return vipSeatSelectedImage;
    }

    public int getVipSeatX() {
        return vipSeatX;
    }

    public int getVipSeatY() {
        return vipSeatY;
    }

    public int getVipSeatWidth() {
        return vipSeatWidth;
    }

    public String getVipSeatImage() {
        return vipSeatImage;
    }

    public int getLoveSeatX() {
        return loveSeatX;
    }

    public int getLoveSeatY() {
        return loveSeatY;
    }

    public int getLoveSeatWidth() {
        return loveSeatWidth;
    }

    public int getLoveSeatHeight() {
        return loveSeatHeight;
    }

    public int getLoveSeatPrice() {
        return loveSeatPrice;
    }

    public String getLoveSeatImage() {
        return loveSeatImage;
    }

    public String getLoveSeatSelectedImage() {
        return loveSeatSelectedImage;
    }

    public String getNormalSeatSelectedImage() {
        return normalSeatSelectedImage;
    }

    public String getNormalSeatImage() {
        return normalSeatImage;
    }

    public int getNormalSeatPrice() {
        return normalSeatPrice;
    }

    public int getNormalSeatX() {
        return normalSeatX;
    }

    public int getNormalSeatY() {
        return normalSeatY;
    }

    public int getNormalSeatWidth() {
        return normalSeatWidth;
    }

    public int getNormalSeatHeight() {
        return normalSeatHeight;
    }

    public void setNormalSeatX(int normalSeatX) {
        this.normalSeatX = normalSeatX;
    }

    public void setNormalSeatY(int normalSeatY) {
        this.normalSeatY = normalSeatY;
    }
}
