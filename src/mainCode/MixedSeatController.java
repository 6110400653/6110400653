package mainCode;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MixedSeatController {
    private Ticket t1 = new Ticket() ;
    private MasterSeat seatData = new MasterSeat() ;
    private Customer customer ;
    private BookingData bookingData ;
    private ArrayList<Customer> customers ;

    @FXML
    private ArrayList<ToggleButton> seatNormal = new ArrayList<>() ;
    @FXML
    private ArrayList<ToggleButton> seatVip = new ArrayList<>() ;
    @FXML
    private ArrayList<ToggleButton> seatLove = new ArrayList<>() ;

    @FXML
    private Label ticketInfo ;

    @FXML
    AnchorPane pane ;

    public void load(){
        int i = 0 ;
        while(i < 16){
            seatNormal.add(new ToggleButton()) ;
            i++ ;
        }
        i = 0 ;
        while(i<12){
            seatVip.add(new ToggleButton()) ;
            i++ ;
        }
        i = 0 ;
        while(i<4){
            seatLove.add(new ToggleButton()) ;
            i++ ;
        }
    }

    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                int i = 1 ;
                int x = seatData.getLoveSeatX() ;
                int y = seatData.getLoveSeatY() ;
                load() ;
                for(ToggleButton s: seatLove){
//            s = new ToggleButton() ;
                    s.setId(i+"");
                    s.setLayoutX(x);
                    s.setLayoutY(y);
                    s.setPrefSize(seatData.getLoveSeatWidth(), seatData.getLoveSeatHeight());
                    s.setOnAction(this::swapLoveSeatAction);
                    ImageView m = new ImageView("/asset/love"+i+".png") ;
                    m.setFitHeight(seatData.getLoveSeatHeight()-15);
                    m.setFitWidth(seatData.getLoveSeatWidth()-20);
                    s.setGraphic(m);

//            s.getChildrenUnmodifiable().add(new ImageView("/asset/normalSeat.png")) ;
                    x += seatData.getLoveSeatWidth() ;
                    if(i%4 == 0){
                        x = seatData.getLoveSeatX() ;
                        y += seatData.getLoveSeatHeight() ;
                    }
                    if(i <= 4){
                        System.out.println(t1.getMovieName());
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "A"+i)){
                            s.setDisable(true);
                            ImageView m2 = new ImageView(seatData.getDisableSeatLove()) ;
                            m2.setFitHeight(seatData.getLoveSeatHeight()-15);
                            m2.setFitWidth(seatData.getLoveSeatWidth()-20);
                            s.setGraphic(m2);
                        }
                    }
                    pane.getChildren().add(s) ;
                    i += 1 ;
                }
                i = 1 ;
                x =  seatData.getVipSeatX();
                y = seatData.getVipSeatY() ;
                int image = 1 ;
                int numRow = 1 ;
                for(ToggleButton s: seatVip){
                    image = i%6 ;
                    if(image == 0){
                        image = 6 ;
                    }
                    s.setId(i+"");
                    s.setLayoutX(x);
                    s.setLayoutY(y);
                    s.setPrefSize(seatData.getVipSeatWidth(), seatData.getVipSeatHeight());
                    s.setOnAction(this::swapVipSeatAction);
                    ImageView m = new ImageView("/asset/"+image+"vip.png") ;
                    m.setFitHeight(seatData.getVipSeatHeight()-15);
                    m.setFitWidth(seatData.getVipSeatWidth()-20);
                    s.setGraphic(m);

//            s.getChildrenUnmodifiable().add(new ImageView("/asset/normalSeat.png")) ;
                    x += seatData.getVipSeatWidth() ;
                    if(i%6 == 0){
                        x = seatData.getVipSeatX() ;
                        y += seatData.getVipSeatHeight() ;
                    }
                    if(numRow <= 6){
                        System.out.println(t1.getMovieName());
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "B"+image)){
                            s.setDisable(true);
                            ImageView m2 = new ImageView(seatData.getDisableSeat()) ;
                            m2.setFitHeight(seatData.getVipSeatHeight()-15);
                            m2.setFitWidth(seatData.getVipSeatWidth()-20);
                            s.setGraphic(m2);
                        }
                    }
                    else if(numRow <= 12){
                        System.out.println(t1.getMovieName());
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "C"+image)){
                            s.setDisable(true);
                            ImageView m2 = new ImageView(seatData.getDisableSeat()) ;
                            m2.setFitHeight(seatData.getVipSeatHeight()-15);
                            m2.setFitWidth(seatData.getVipSeatWidth()-20);
                            s.setGraphic(m2);
                        }
                    }
                    pane.getChildren().add(s) ;
                    i += 1 ;
                    numRow++ ;
                }
                i = 1 ;
                seatData.setNormalSeatX(65);
                seatData.setNormalSeatY(150);
                x =  seatData.getNormalSeatX();
                y = seatData.getNormalSeatY() ;
                numRow = 1 ;
                for(ToggleButton s: seatNormal){
                    image = i%8 ;
                    if(image == 0){
                        image = 8 ;
                    }
                    s.setId(i+"");
//            s = new ToggleButton() ;
                    s.setLayoutX(x);
                    s.setLayoutY(y);
                    s.setPrefSize(seatData.getNormalSeatWidth(), seatData.getNormalSeatHeight());
                    s.setOnAction(this::swapNormalSeatAction);
                    ImageView m = new ImageView("/asset/"+image+"normal.png") ;
                    m.setFitHeight(seatData.getNormalSeatHeight()-15);
                    m.setFitWidth(seatData.getNormalSeatWidth()-20);
                    s.setGraphic(m);

//            s.getChildrenUnmodifiable().add(new ImageView("/asset/normalSeat.png")) ;
                    x += seatData.getNormalSeatWidth() ;
                    if(i%8 == 0){
                        x = seatData.getNormalSeatX() ;
                        y += seatData.getNormalSeatHeight() ;
                    }
                    if(numRow <= 8){
                        System.out.println("i = "+ i);
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "D"+image)){
                            s.setDisable(true);
                            ImageView m2 = new ImageView(seatData.getDisableSeat()) ;
                            m2.setFitHeight(seatData.getNormalSeatHeight()-15);
                            m2.setFitWidth(seatData.getNormalSeatWidth()-20);
                            s.setGraphic(m2);
                        }
                    }
                    else if(numRow <= 16){
                        System.out.println("i = "+ i);
                        if(bookingData.findReserved(t1.getTheatre().getNumber(), t1.getShowTime(), "E"+image)){
                            s.setDisable(true);
                            ImageView m2 = new ImageView(seatData.getDisableSeat()) ;
                            m2.setFitHeight(seatData.getNormalSeatHeight()-15);
                            m2.setFitWidth(seatData.getNormalSeatWidth()-20);
                            s.setGraphic(m2);
                        }
                    }
                    pane.getChildren().add(s) ;
                    i += 1 ;
                    numRow++ ;
                }

            }

            private void swapVipSeatAction(ActionEvent e) {
                ToggleButton s = (ToggleButton) e.getSource() ;
                if(s.isSelected()){
                    ImageView imageView = new ImageView(seatData.getVipSeatSelectedImage()) ;
                    imageView.setFitWidth(seatData.getVipSeatWidth()-20);
                    imageView.setFitHeight(seatData.getVipSeatHeight()-15);
                    s.setGraphic(imageView);
                }
                else{
                    int image = 1 ;
                        image = Integer.parseInt(s.getId())%6 ;
                        if(image == 0){
                            image = 6 ;
                        }
                    ImageView imageView = new ImageView("/asset/"+image+"vip.png") ;
                    imageView.setFitWidth(seatData.getVipSeatWidth()-20);
                    imageView.setFitHeight(seatData.getVipSeatHeight()-15);
                    s.setGraphic(imageView);
                }
            }

            private void swapNormalSeatAction(ActionEvent e) {
                ToggleButton s = (ToggleButton) e.getSource() ;
                if(s.isSelected()){
                    ImageView imageView = new ImageView(seatData.getNormalSeatSelectedImage()) ;
                    imageView.setFitWidth(seatData.getNormalSeatWidth()-20);
                    imageView.setFitHeight(seatData.getNormalSeatHeight()-13);
                    s.setGraphic(imageView);
                }
                else{
                    int image = 1 ;
                        image = Integer.parseInt(s.getId())%8 ;
                        if(image == 0) {
                            image = 8;
                        }
//                        s.setId(i+"");
                    ImageView imageView = new ImageView("/asset/"+image+"normal.png") ;
                    imageView.setFitWidth(seatData.getNormalSeatWidth()-20);
                    imageView.setFitHeight(seatData.getNormalSeatHeight()-15);
                    s.setGraphic(imageView);
                }
            }

            private void swapLoveSeatAction(ActionEvent e) {
                ToggleButton s = (ToggleButton) e.getSource();
                if (s.isSelected()) {
                    ImageView imageView = new ImageView(seatData.getLoveSeatSelectedImage());
                    imageView.setFitWidth(seatData.getLoveSeatWidth() - 20);
                    imageView.setFitHeight(seatData.getLoveSeatHeight() - 15);
                    s.setGraphic(imageView);
                } else {
                    int image = 1 ;
                        image = Integer.parseInt(s.getId())%4 ;
                        if(image == 0){
                            image = 4 ;
                        }
//                        s.setId(i+"");
                    ImageView imageView = new ImageView("/asset/love"+image+".png");
                    imageView.setFitWidth(seatData.getLoveSeatWidth() - 20);
                    imageView.setFitHeight(seatData.getLoveSeatHeight() - 15);
                    s.setGraphic(imageView);

                }
            }
        });


    }

    @FXML void acceptBtnAction(ActionEvent event) throws IOException {
        int i = 1;
        int x = 1;
        int numSeatLove = 0 ;
        for (ToggleButton s : seatLove) {
            if (s.isSelected() && i <= 4) {
                t1.addSeat("A" + x);
                numSeatLove++ ;
            }
            i++;
            x++;
        }
        i = 1;
        x = 1;
        int numSeatVip = 0 ;
        for (ToggleButton s : seatVip) {
            if (s.isSelected() && i <= 6) {
                t1.addSeat("B" + x);
                numSeatVip++ ;
            } else if (s.isSelected() && i <= 12) {
                t1.addSeat("C" + x);
                numSeatVip++ ;
            }
            if (x % 6 == 0) {
                x = 0;
            }
            i++;
            x++;
        }
        x = 1 ;
        i = 1 ;
        int numSeatNormal = 0 ;
        for (ToggleButton s: seatNormal) {
            if (s.isSelected() && i <= 8) {
                t1.addSeat("D" + x);
                numSeatNormal++ ;
            }
            else if(s.isSelected() && i <= 16){
                t1.addSeat("E" + x);
                numSeatNormal++ ;
            }

            if (x % 8 == 0) {
                 x = 0;
            }
            i++;
            x++;
        }

        double price = numSeatLove*seatData.getLoveSeatPrice() + numSeatNormal*seatData.getNormalSeatPrice() + numSeatVip*seatData.getVipSeatPrice() ;
        if(t1.getTheatre().getSystem().equals("4K")){
            price += 30 ;
        }
        else if(t1.getTheatre().getSystem().equals("3D")){
            price += 50 ;
        }

        Button b2 = (Button) event.getSource();
        Stage stage = (Stage) b2.getScene().getWindow() ;
        FXMLLoader loader2 = new FXMLLoader(getClass().getResource("ticketShow.fxml")) ;
        stage.setScene(new Scene(loader2.load(), 720, 400));
        TicketShowController ticketShowController = loader2.getController() ;
        t1.setPrice(price);
        ticketShowController.setT1(t1);
        customer.addTicket(t1);
        bookingData.addData(t1);
        ticketShowController.setCustomer(customer);
        ticketShowController.setBookingData(bookingData);
        ticketShowController.setCustomers(customers);
        writeFile();
        stage.show();
    }


    @FXML
    public void backBtnAction(ActionEvent event) throws IOException {
        Button b2 = (Button) event.getSource() ;
        Stage stage = (Stage) b2.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        HomeController homeController = loader.getController() ;
        homeController.setBookingData(bookingData);
        homeController.setCustomer(customer);
        stage.show();
    }

    public void setT1(Ticket t1) {
        this.t1 = t1;
//        ticketInfo.setText("Theater: "+t1.getTheaterNumber()+"  Movie: "+t1.getMovieName()+"  Showtime: "+t1.getShowTime());
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setTicketInfo(String text) {
        ticketInfo.setText(text);
    }

    public void setBookingData(BookingData bookingData) {
        this.bookingData = bookingData;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public void writeFile() throws IOException {
        String customerData = "" ;
        for(Customer customer:customers){
            customerData += customer.getName() + "," + customer.getId() + "," + customer.getPass() + "," + customer.getMail() + "\n" ;
            for(Ticket ticket:customer.getTickets()){
                customerData += ticket.getTheatreNumber() + "," + ticket.getShowTime() + "," ;
                for(String seat:ticket.getSeat()){
                    customerData += seat + " " ;
                }
                customerData += "\n" ;
            }
        }

        FileWriter writer = new FileWriter("BookingData.csv");
        writer.write(customerData);
        writer.close();
//        System.out.println(customerData);
    }

    @FXML
    public void swapLoveSeatAction(ActionEvent e){
        ToggleButton s = (ToggleButton) e.getSource() ;
        if(s.isSelected()){
            ImageView imageView = new ImageView(seatData.getLoveSeatSelectedImage()) ;
            imageView.setFitWidth(seatData.getLoveSeatWidth()-20);
            imageView.setFitHeight(seatData.getLoveSeatHeight()-15);
            s.setGraphic(imageView);
        }
        else{
            ImageView imageView = new ImageView(seatData.getLoveSeatImage()) ;
            imageView.setFitWidth(seatData.getLoveSeatWidth()-20);
            imageView.setFitHeight(seatData.getLoveSeatHeight()-15);
            s.setGraphic(imageView);
        }
    }

    @FXML
    public void swapVipSeatAction(ActionEvent e){
        ToggleButton s = (ToggleButton) e.getSource() ;
        if(s.isSelected()){
            ImageView imageView = new ImageView(seatData.getVipSeatSelectedImage()) ;
            imageView.setFitWidth(seatData.getVipSeatWidth()-20);
            imageView.setFitHeight(seatData.getVipSeatHeight()-15);
            s.setGraphic(imageView);
        }
        else{
            ImageView imageView = new ImageView(seatData.getVipSeatImage()) ;
            imageView.setFitWidth(seatData.getVipSeatWidth()-20);
            imageView.setFitHeight(seatData.getVipSeatHeight()-15);
            s.setGraphic(imageView);
        }
    }

    @FXML
    public void swapNormalSeatAction(ActionEvent e){
        ToggleButton s = (ToggleButton) e.getSource() ;
        if(s.isSelected()){
            ImageView imageView = new ImageView(seatData.getNormalSeatSelectedImage()) ;
            imageView.setFitWidth(seatData.getNormalSeatWidth()-20);
            imageView.setFitHeight(seatData.getNormalSeatHeight()-13);
            s.setGraphic(imageView);
        }
        else{
            ImageView imageView = new ImageView(seatData.getNormalSeatImage()) ;
            imageView.setFitWidth(seatData.getNormalSeatWidth()-20);
            imageView.setFitHeight(seatData.getNormalSeatHeight()-15);
            s.setGraphic(imageView);
        }
    }

}
