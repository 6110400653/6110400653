package mainCode;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MovieInfoController {

    private Ticket t1 ;
    private Customer customer ;
    private ArrayList<Customer> customers ;
    private BookingData bookingData ;

    @FXML
    MediaView trailer ;

    @FXML
    Label movieName, info, movieLen ;

    @FXML
    Button backBtn ;

    @FXML
    ImageView poster ;

    @FXML
    MediaPlayer mediaPlayer ;

    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Media media = new Media(new File(t1.getMovie().getTrailer()).toURI().toString()) ;
                mediaPlayer = new MediaPlayer(media) ;
                trailer.setMediaPlayer(mediaPlayer);
                mediaPlayer.setAutoPlay(true);
                movieName.setText(t1.getMovie().getName());
                info.setText(t1.getMovie().getDescription());
                poster.setImage(new Image(t1.getMovie().getPoster()));
                movieLen.setText(t1.getMovie().getLength()+" Hr");

            }
        });

    }

    public void setT1(Ticket t1) {
        this.t1 = t1;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public void setBookingData(BookingData bookingData) {
        this.bookingData = bookingData;
    }

    public void backBtnAction(ActionEvent event) throws IOException {
        mediaPlayer.stop();
        javafx.scene.control.Button b2 = (javafx.scene.control.Button) event.getSource() ;
        Stage stage = (Stage) b2.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        HomeController homeController = loader.getController() ;
        homeController.setCustomer(customer);
        homeController.setBookingData(bookingData);
        homeController.setCustomers(customers);
        stage.show();
    }
}
