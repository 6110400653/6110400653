package mainCode;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LoginController {

    private ArrayList<Customer> customers = new ArrayList<>() ;
    private BookingData bookingData = new BookingData() ;

    @FXML
    TextField username, password ;

    @FXML
    Button loginBtn, registerBtn, creditBtn ;

    @FXML
    Label loginLog ;

    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    BufferedReader reader = new BufferedReader(new FileReader("BookingData.csv")) ;

                    String  line  ;
                    int i = -1 ;
                    while((line = reader.readLine()) != null){
                        String[] lineSplit = line.split(",") ;
                        if(lineSplit.length == 4){
                            customers.add(new Customer(lineSplit[0], lineSplit[1], lineSplit[2], lineSplit[3]));
                            i++ ;
                        }
                        else if(lineSplit.length == 3){
                            Ticket t = new Ticket() ;
                            t.setTheatreNumber(Integer.parseInt(lineSplit[0]));
                            t.setShowTime(lineSplit[1]);
                            for(String s:lineSplit[2].split(" ")){
                                t.addSeat(s);
                            }
                            bookingData.addData(t);
                            customers.get(i).addTicket(t);
                        }
                        System.out.println(line);
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
//        Ticket t = new Ticket("Avanger", "13.00") ;
//        t.addSeat("A1");
//        t.addSeat("E8"); ;
//        t.setTheatre(new Theatre(1, new Movie(), "3D"));
//        customers.add(new Customer("pang", "pangId", "pangPass", "mail")) ;
//        customers.get(0).addTicket(t);
    }

    @FXML
    public void loginBtnAction(ActionEvent event) throws IOException {
        String user =  username.getText() ;
        String pass = password.getText() ;
        Customer currentUser = checkUser(user) ;
        if(currentUser != null){
            if(currentUser.getPass().equals(pass)){
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow() ;
                FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml")) ;
                stage.setScene(new Scene(loader.load(), 720, 400));
                HomeController homeController = loader.getController() ;
                homeController.setCustomer(currentUser);
                homeController.setBookingData(bookingData);
                homeController.setCustomers(customers);
                stage.show();
            }
            else {
                loginLog.setText("Incorrect Password!!");
            }
        }
        else {
            loginLog.setText("Couldn't find your Account");
        }

    }

    public Customer checkUser(String user){
        for(Customer c: customers){
//            System.out.println("Name:" +c.getName()+ " Pass:"+c.getPass());
            if(c.getId().equals(user)){
                return c ;
            }
        }
        return null ;
    }

    public void registerBtnAction(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("register.fxml")) ;
        stage.setScene(new Scene(loader.load(), 720, 400));
        RegisterController registerController = loader.getController() ;
//        seatSelectController.setT1(new Ticket("Avanger", "A1", 1));
        registerController.setCustomers(customers);
        stage.show();
    }

    public void creditBtnAction(ActionEvent e) throws IOException {
        {
            Button b = (Button) e.getSource();
            Stage stage = (Stage) b.getScene().getWindow() ;
            FXMLLoader loader = new FXMLLoader(getClass().getResource("credit.fxml")) ;
            stage.setScene(new Scene(loader.load(), 720, 400));
//        seatSelectController.setT1(new Ticket("Avanger", "A1", 1));
            stage.show();
        }
    }

}
