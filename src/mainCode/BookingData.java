package mainCode;

import java.util.ArrayList;

public class BookingData {
    private ArrayList<Ticket> tickets = new ArrayList<>() ;

    public void addData(Ticket ticket){
        tickets.add(ticket) ;
    }

    public boolean findReserved(int theatreNumber, String showTime, String seat){
//        System.out.println("test");
        for(Ticket ticket:tickets){
            if(ticket.getTheatreNumber() == theatreNumber){
                if(ticket.getShowTime().equals(showTime)){
                    for(String s:ticket.getSeat()){
                        if(s.equals(seat)){
                            return true ;
                        }
                    }
                }
            }
        }
        return false ;
    }

    public ArrayList<Ticket> getTickets() {
        return tickets;
    }
}
