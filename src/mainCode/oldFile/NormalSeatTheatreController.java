package mainCode.oldFile;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;
import mainCode.Ticket;
import mainCode.TicketShowController;

import java.io.IOException;

public class NormalSeatTheatreController {
    Ticket t1 ;

    @FXML
    Label ticketInfo ;

    @FXML
    Button acceptBtn, backBtn;

    @FXML
    ToggleButton a1,a2,a3,a4,a5,a6,a7,a8,b1,b2,b3,b4,b5,b6,b7,b8,c1,c2,c3,c4,c5,c6,c7,c8,d1,d2,d3,d4,d5,d6,d7,d8 ;

    @FXML
    public void initialize(){
    }

    public void setT1(Ticket t1) {
        this.t1 = t1;
//        ticketInfo.setText("Theater: "+t1.getTheaterNumber()+"  Movie: "+t1.getMovieName()+"  Showtime: "+t1.getShowTime());
    }
    public void setTicketInfo(String text){
        ticketInfo.setText(text);
    }

    @FXML public void acceptBtnAction(ActionEvent event) throws IOException {
        if(a1.isSelected()){
            t1.addSeat("A1");
        }
        if(a2.isSelected()){
            t1.addSeat("A2");
        }
        if(a3.isSelected()){
            t1.addSeat("A3");
        }
        if(a4.isSelected()){
            t1.addSeat("A4");
        }
        if(a5.isSelected()){
            t1.addSeat("A5");
        }
        if(a6.isSelected()){
            t1.addSeat("A6");
        }
        if(a7.isSelected()){
            t1.addSeat("A7");
        }
        if(a8.isSelected()){
            t1.addSeat("A8");
        }
        if(b1.isSelected()){
            t1.addSeat("B1");
        }
        if(b2.isSelected()){
            t1.addSeat("B2");
        }
        if(b3.isSelected()){
            t1.addSeat("B3");
        }
        if(b4.isSelected()){
            t1.addSeat("B4");
        }
        if(b5.isSelected()){
            t1.addSeat("B5");
        }
        if(b6.isSelected()){
            t1.addSeat("B6");
        }
        if(b7.isSelected()){
            t1.addSeat("B7");
        }
        if(b8.isSelected()){
            t1.addSeat("B8");
        }
        if(c1.isSelected()){
            t1.addSeat("C1");
        }
        if(c2.isSelected()){
            t1.addSeat("C2");
        }
        if(c3.isSelected()){
            t1.addSeat("C3");
        }
        if(c4.isSelected()){
            t1.addSeat("C4");
        }
        if(c5.isSelected()){
            t1.addSeat("C5");
        }
        if(c6.isSelected()){
            t1.addSeat("C6");
        }
        if(c7.isSelected()){
            t1.addSeat("C7");
        }
        if(c8.isSelected()){
            t1.addSeat("C8");
        }
        if(d1.isSelected()){
            t1.addSeat("D1");
        }
        if(d2.isSelected()){
            t1.addSeat("D2");
        }
        if(d3.isSelected()){
            t1.addSeat("D3");
        }
        if(d4.isSelected()){
            t1.addSeat("D4");
        }
        if(d5.isSelected()){
            t1.addSeat("D5");
        }
        if(d6.isSelected()){
            t1.addSeat("D6");
        }
        if(d7.isSelected()){
            t1.addSeat("D7");
        }
        if(d8.isSelected()){
            t1.addSeat("D8");
        }

        Button b2 = (Button) event.getSource();
        Stage stage = (Stage) b2.getScene().getWindow() ;
        FXMLLoader loader2 = new FXMLLoader(getClass().getResource("ticketShow.fxml")) ;
        stage.setScene(new Scene(loader2.load(), 600, 400));
        TicketShowController ticketShowController = loader2.getController() ;
        ticketShowController.setT1(new Ticket(t1.getSeat(), t1.getMovieName(), t1.getShowTime(), t1.getTheatre()));
        stage.show();

    }

    @FXML
    public void backBtnAction(ActionEvent event) throws IOException {
        Button b2 = (Button) event.getSource() ;
        Stage stage = (Stage) b2.getScene().getWindow() ;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml")) ;
        stage.setScene(new Scene(loader.load(), 600, 400));
        stage.show();
    }
}
